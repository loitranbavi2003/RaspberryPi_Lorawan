#include <Arduino.h>
#include <stdio.h>
#include <string.h>

#define DEBUG     true
#define BAUDRATE  9600
#define TIMESETUP 2000

	// Set the Device EUI
#define DEUI "77:F7:87:D7:BB:03:ED:30"
	// Set the App Eui
#define APPEUI "00:00:00:00:00:00:00:00"
	// Set the Application Key
#define APPKEY "c2:57:3c:24:bc:63:a6:2d:01:f5:2a:4f:6e:16:52:26"
    // Set the Device address
#define DADDR "01:89:aa:78"
	// Set the Network Session Key
#define NWKSKEY "15:e4:3a:19:78:0e:07:17:f3:ac:40:75:5c:0c:70:f9"
	// Set the Application Session Key
#define APPSKEY "f0:c0:9d:f1:96:6c:00:7c:7e:bc:71:59:2d:da:34:75"

String sendData(String command, const int timeout, boolean debug);

void setup()
{
    Serial2.begin(BAUDRATE);
    Serial.begin(BAUDRATE);
    Serial.println("Now turnning the Lorawan on.Please wait.");

    Serial.println("Init over");
    
    sendData("AT+DEUI=" + String(DEUI), TIMESETUP, DEBUG);
    sendData("AT+APPEUI=" + String(APPEUI), TIMESETUP, DEBUG);
    sendData("AT+APPKEY=" + String(APPKEY), TIMESETUP, DEBUG);

    sendData("AT+DADDR=" + String(DADDR), TIMESETUP, DEBUG);
    sendData("AT+NWKSKEY=" + String(NWKSKEY), TIMESETUP, DEBUG);
    sendData("AT+APPSKEY=" + String(APPSKEY), TIMESETUP, DEBUG);

    sendData("AT+JOIN=0:1:8:10", TIMESETUP, DEBUG);
}

long int runtime = 20000;
void loop()
{
    if ((millis() - runtime) > 20000)
    {
        char msgss[30] = "";
        sprintf(msgss, "AT+SEND=1:1:0x01");
        Serial.println("------------------------------------ Send DATA -------------------------------");
        Serial.println(msgss);
        Serial2.println(msgss);
        
        runtime = millis();
    }

    while (Serial2.available() > 0)
    {
        Serial.write(Serial2.read());
        yield();
    }
}

String sendData(String command, const int timeout, boolean debug)
{
    String response = "";
    Serial2.println(command);

    long int time = millis();
    while ((time + timeout) > millis())
    {
        while (Serial2.available())
        {
            char c = Serial2.read();
            response += c;
        }
    }
    if (debug)
    {     
        Serial.print(response);
    }
    return response;
}
