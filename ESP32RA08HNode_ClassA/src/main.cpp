#include <Arduino.h>
#include <stdio.h>
#include <string.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <MQ135.h>

#define DEBUG true

// DHT sensor pin
#define BUTTON1 27
#define DHTPIN 4
#define light_sensor 25
#define DHTTYPE DHT11
#define PIN_MQ135 26
DHT dht(DHTPIN, DHTTYPE);
MQ135 mq135_sensor(PIN_MQ135);
// get appeui and appkey from the thing network
#define DEVEUI "77F787D7BB03ED30"
#define APPEUI "0000000000000000"
#define APPKEY "C2573C24BC63A62D01F52A4F6E165226"
////////////
// #define DEVEUI "B6BD2FBB7D7400BD"
// #define APPEUI "0000000000000000"
// #define APPKEY "F111A1F79392907F8423E25146A434E7"

int led_flag = 0;
int led_state = 0;
const int LED_PIN = 22; // GPIO22
const int GND_PIN = 21;
void pin_init();
void updateLedState(int newLedState);
String sendData(String command, const int timeout, boolean debug);
String lorawan_tx_rx(String msg_hex, const int timeout);

int baudrate = 9600;
void setup()
{
    Serial2.begin(baudrate);
    Serial.begin(baudrate);
    Wire.begin();
    dht.begin();
    pinMode(LED_PIN, OUTPUT);
    pinMode(GND_PIN, OUTPUT);
    digitalWrite(GND_PIN, LOW);
    Serial.println("Now turnning the Lorawan on.Please wait.");

    Serial.println("Init over");
    int timesetup = 2000;
    sendData("AT+CDEVEUI=" + String(DEVEUI), timesetup, DEBUG);
    sendData("AT+CAPPEUI=" + String(APPEUI), timesetup, DEBUG);
    sendData("AT+CAPPKEY=" + String(APPKEY), timesetup, DEBUG);
    //option------
    sendData("AT+CULDLMODE=2", timesetup, DEBUG);
    //------------
    sendData("AT+CJOINMODE=0", timesetup, DEBUG);
    sendData("AT+CCLASS=0", timesetup, DEBUG);
    sendData("AT+CADR=0", timesetup, DEBUG);
    sendData("AT+CDATARATE=5", timesetup, DEBUG);
    sendData("AT+CJOIN=1,0,10,10", timesetup, DEBUG);
}

long int runtime = 20000;
void loop()
{
    if ((millis() - runtime) > 10000)
    {
        //////////
        // int hum=random(101);
        // int temp=random(101);
        // Read DHT11 sensor data
        int humid = (int)dht.readHumidity();
        int temp = (int)dht.readTemperature();
        int lightRaw = analogRead(light_sensor);
        int light =100 - map(lightRaw, 0, 4095, 0, 100);
        int dustppm = mq135_sensor.getPPM();
        int dust = dustppm/10000;
        // int hum = (int)humidity;
        // int temp = (int)temperature;
        // int temp = 12;
        // int humid = 34;
        // int light= 56;
        // int dust = 78;
        char msgss[30] = "";
        sprintf(msgss, "AT+DTRX=1,2,10,%02x%02x%02x%02x%02x", temp, humid, light, dust, led_state);
        // sprintf(msgss, "AT+DTRX=1,2,5,%02x%04x", humid, temp);
        Serial.println(msgss);
        sendData((String)msgss, 0, DEBUG);
        //////////
        String downlink = "";
        if (led_flag == 1)
            downlink = lorawan_tx_rx("F8", 20000);
        if (led_flag == 0)
            downlink = lorawan_tx_rx("F7", 20000);

        if (downlink.endsWith("F8"))
            led_flag = 1;
        if (downlink.endsWith("F7"))
            led_flag = 0;
        Serial.println(downlink);
        if (led_flag == 1)
        {
            digitalWrite(BUTTON1, 1);
            Serial.println("LED ON");
            updateLedState(1);
        }
        else
        {
            digitalWrite(BUTTON1, 0);
            Serial.println("LED OFF");
            updateLedState(0);
        }
        Serial.println("--------------------------------------------------------------------");

        runtime = millis();
    }
    digitalWrite(LED_PIN, led_state);
    while (Serial2.available() > 0)
    {
        Serial.write(Serial2.read());
        yield();
    }
    // while (Serial.available() > 0)
    // {
    //     Serial2.write(Serial.read());
    //     yield();
    // }
}

String sendData(String command, const int timeout, boolean debug)
{
    String response = "";
    Serial2.println(command);
    long int time = millis();
    while ((time + timeout) > millis())
    {
        while (Serial2.available())
        {
            char c = Serial2.read();
            response += c;
        }
    }
    if (debug)
    {
        Serial.print(response);
    }
    return response;
}

String lorawan_tx_rx(String msg_hex, const int timeout)
{
    String response = "";
    String rec_head = "OK+RECV:";

    char msg[50] = "";
    sprintf(msg, "AT+DTRX=1,2,5,%s", msg_hex.c_str());

    Serial.println(msg);
    Serial2.println(msg);

    long int time = millis();
    while ((time + timeout) > millis())
    {
        while (Serial2.available())
        {
            char c = Serial2.read();

            if (c == '\r')
                continue;
            else if (c == '\n')
            {
                Serial.println(response);
                
                if (response.indexOf(rec_head) != -1)
                {
                    Serial.println("-----------Get downlink msg-----------");

                    String result = response.substring(response.indexOf(rec_head) + rec_head.length());

                    Serial.println(result);
                    Serial.println("-----------Over-----------");
                    return result;
                }
                else if (response.indexOf("ERR+SENT") != -1)
                {
                    Serial.println("-----------Get downlink msg-----------");

                    String result = "SEND FAILED";

                    Serial.println(result);
                    Serial.println("-----------Over-----------");
                    return result;
                }

                response = "";
            }
            else
                response += c;
        }
    }
    Serial.println(response);

    return "";
}
void updateLedState(int newLedState)
{
    // Cập nhật giá trị led_state
    led_state = newLedState;
}