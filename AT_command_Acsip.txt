
------------------ OTAA ------------------
	Set the Device EUI
AT+DEUI=77:F7:87:D7:BB:03:ED:30<CR>
	Set the App Eui
AT+APPEUI=00:00:00:00:00:00:00:00<CR>
	Set the Application Key
AT+APPKEY=c2:57:3c:24:bc:63:a6:2d:01:f5:2a:4f:6e:16:52:26<CR>

------------------ ABP -------------------
	Set the Device address
AT+DADDR=01:89:aa:78<CR>
	Set the Network Session Key
AT+NWKSKEY=15:e4:3a:19:78:0e:07:17:f3:ac:40:75:5c:0c:70:f9<CR>
	Set the Application Session Key
AT+APPSKEY=f0:c0:9d:f1:96:6c:00:7c:7e:bc:71:59:2d:da:34:75<CR>

------------------ JOIN ------------------
	Join network with Mode=[0:ABP, 1:OTAA]
AT+JOIN=0:1:10:10<CR> 
	Send binary data with the application Port=[1..199] and Ack=[0:unconfirmed, 1:confirmed]
	AT+SEND=<Port>:<Ack>:<Payload><CR>.
AT+SEND=1:1:0x01<CR>
	Get the Device Class
AT+CLASS=?


----------------------------------------------------------------------------------------------------

APPLICATION_VERSION: V1.2.0
MW_LORAWAN_VERSION:  V2.4.0
MW_RADIO_VERSION:    V1.2.0
L2_SPEC_VERSION:     V1.0.4
RP_SPEC_VERSION:     V2-1.0.1
###### OTAA ######
###### AppKey:      2B:7E:15:16:28:AE:D2:A6:AB:F7:15:88:09:CF:4F:3C
###### NwkKey:      2B:7E:15:16:28:AE:D2:A6:AB:F7:15:88:09:CF:4F:3C
###### ABP  ######
###### AppSKey:     2B:7E:15:16:28:AE:D2:A6:AB:F7:15:88:09:CF:4F:3C
###### NwkSKey:     2B:7E:15:16:28:AE:D2:A6:AB:F7:15:88:09:CF:4F:3C
###### IDs  ######
###### DevEui:      00:80:E1:15:00:07:20:21
###### AppEui:      01:01:01:01:01:01:01:01
###### DevAddr:     00:07:20:21
ATtention command interface
AT? to list all available functions

----------------------------------------------------------------------------------------------------

AT?
AT+<CMD>?        : Help on <CMD>
AT+<CMD>         : Run <CMD>
AT+<CMD>=<value> : Set the value
AT+<CMD>=?       : Get the value
ATZ Trig a MCU reset
AT+RFS: Restore EEPROM Factory Settings
AT+CS: Store current context to EEPROM
AT+VL=<Level><CR>. Set the Verbose Level=[0:Off .. 3:High]
AT+LTIME Get the local time in UTC format
AT+APPEUI=<XX:XX:XX:XX:XX:XX:XX:XX><CR>. Get or Set the App Eui
AT+NWKKEY=<XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX><CR>: Get or Set the Network Key
AT+APPKEY=<XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX><CR>: Get or Set the Application Key
AT+NWKSKEY=<XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX><CR>: Get or Set the Network Session Key
AT+APPSKEY=<XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX><CR>: Get or Set the Application Session Key
AT+DADDR=<XXXXXXXX><CR>. Get or Set the Device address
AT+DEUI=<XX:XX:XX:XX:XX:XX:XX:XX><CR>. Get or Set the Device EUI
AT+NWKID=<NwkID><CR>. Get or Set the Network ID=[0..127]
AT+JOIN=<Mode><CR>. Join network with Mode=[0:ABP, 1:OTAA]
AT+LINKC. Piggyback a Link Check Request to the next uplink
AT+SEND=<Port>:<Ack>:<Payload><CR>. Send binary data with the application Port=[1..199] and Ack=[0:unconfirmed, 1:confirmed]
AT+VER Get the FW version
AT+ADR=<ADR><CR>. Get or Set the Adaptive Data Rate setting ADR=[0:off, 1:on]
AT+DR=<DataRate><CR>. Get or Set the Tx DataRate=[0..7]
AT+BAND=<BandID><CR>. Get or Set the Active Region BandID=[0:AS923, 1:AU915, 2:CN470, 3:CN779, 4:EU433, 5:EU868, 6:KR920, 7:IN865, 8:US915, 9:RU864]
AT+CLASS=<Class><CR>. Get or Set the Device Class=[A, B, C]
AT+DCS=<DutyCycle><CR>. Get or Set the ETSI DutyCycle=[0:disable, 1:enable] - Only for testing
AT+JN1DL=<Delay><CR>. Get or Set the Join Accept Delay between the end of the Tx and the Join Rx Window 1 in ms
AT+JN2DL=<Delay><CR>. Get or Set the Join Accept Delay between the end of the Tx and the Join Rx Window 2 in ms
AT+RX1DL=<Delay><CR>. Get or Set the delay between the end of the Tx and the Rx Window 1 in ms
AT+RX2DL=<Delay><CR>. Get or Set the delay between the end of the Tx and the Rx Window 2 in ms
AT+RX2DR=<DataRate><CR>. Get or Set the Rx2 window DataRate=[0..7]
AT+RX2FQ=<Freq><CR>. Get or Set the Rx2 window Freq in Hz
AT+TXP=<Power><CR>. Get or Set the Transmit Power=[0..15](valid range according to region)
AT+PGSLOT=<Period><CR>. Set or Get the unicast ping slot Period=[0:1s .. 7:128s](=2^Period)
AT+TTONE Starts RF Tone test
AT+TRSSI Starts RF RSSI tone test
AT+TCONF=<Freq in Hz>:<Power in dBm>:<Lora Bandwidth <0 to 6>, or Rx FSK Bandwidth in Hz>:<Lora SF or FSK datarate (bps)>:<CodingRate 4/5, 4/6, 4/7, 4/8>:
         <Lna>:<PA Boost>:<Modulation 0:FSK, 1:Lora, 2:BPSK, 3:MSK>:<PayloadLen in Bytes>:<FskDeviation in Hz>:<LowDrOpt 0:off, 1:on, 2:Auto>:
         <BTproduct: 0:no Gaussian Filter Applied, 1:BT=0,3, 2:BT=0,5, 3:BT=0,7, 4:BT=1><CR>. Configure RF test
AT+TCONF=868000000:14:50000:50000:4/5:0:0:0:16:25000:2:3 /*FSK*/
AT+TCONF=868000000:14:10000:10000:4/5:0:0:3:16:25000:2:3 /*MSK*/
AT+TCONF=868000000:14:4:12:4/5:0:0:1:16:25000:2:3 /*LORA*/
AT+TTX=<PacketNb><CR>. Starts RF Tx test: Nb of packets sent
AT+TRX=<PacketNb><CR>. Starts RF Rx test: Nb of packets expected
AT+CERTIF=<Mode><CR>. Set the module in LoraWan Certification with Join Mode=[0:ABP, 1:OTAA]
AT+TTH=<Fstart>,<Fstop>,<Fdelta>,<PacketNb><CR>. Starts RF Tx hopping test from Fstart to Fstop in Hz or MHz, Fdelta in Hz
AT+TOFF Stops on-going RF test
AT+BAT Get the battery Level in mV

OK
