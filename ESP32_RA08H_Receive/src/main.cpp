#include <Arduino.h>
#include <stdio.h>
#include <string.h>

#define DEBUG     true
#define BAUDRATE  9600
#define TIMESETUP 2000

// get appeui and appkey from the thing network
#define DEVEUI "77F787D7BB03ED30"
#define APPEUI "0000000000000000"
#define APPKEY "C2573C24BC63A62D01F52A4F6E165226"

String sendData(String command, const int timeout, boolean debug);

void setup()
{
    Serial2.begin(BAUDRATE);
    Serial.begin(BAUDRATE);
    Serial.println("Now turnning the Lorawan on.Please wait.");

    Serial.println("Init over");
    
    sendData("AT+CDEVEUI=" + String(DEVEUI), TIMESETUP, DEBUG);
    sendData("AT+CAPPEUI=" + String(APPEUI), TIMESETUP, DEBUG);
    sendData("AT+CAPPKEY=" + String(APPKEY), TIMESETUP, DEBUG);
    //option------
    sendData("AT+CULDLMODE=2", TIMESETUP, DEBUG);
    //------------
    sendData("AT+CJOINMODE=0", TIMESETUP, DEBUG);
    sendData("AT+CCLASS=0", TIMESETUP, DEBUG);
    sendData("AT+CADR=0", TIMESETUP, DEBUG);
    sendData("AT+CDATARATE=5", TIMESETUP, DEBUG);
    sendData("AT+CJOIN=1,0,10,10", TIMESETUP, DEBUG);
}

long int runtime = 20000;
void loop()
{
    if ((millis() - runtime) > 20000)
    {
        char msgss[30] = "";
        sprintf(msgss, "AT+DTRX=1,2,10,1");
        Serial.println("--------------------------------------------------------------------");
        Serial.println(msgss);
        Serial2.println(msgss);
        // sendData((String)msgss, 0, DEBUG);
        runtime = millis();
    }

    while (Serial2.available() > 0)
    {
        Serial.write(Serial2.read());
        yield();
    }
}

String sendData(String command, const int timeout, boolean debug)
{
    String response = "";
    Serial2.println(command);

    long int time = millis();
    while ((time + timeout) > millis())
    {
        while (Serial2.available())
        {
            char c = Serial2.read();
            response += c;
        }
    }
    if (debug)
    {     
        Serial.print(response);
    }
    return response;
}
